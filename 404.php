<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package WP_Bootstrap_Starter
 */

get_header(); ?>

<section id="primary" class="error-page content-area col-sm-12 col-md-12 col-lg-12">
	<main id="main" class="site-main" role="main">

		<section class="container error-404 not-found">
			<header class="page-header">
				<h1 class="page-title"><?php echo __( 'Erreur 404. Cette page n\'existe pas ', 'tbc_theme' ) ?></h1>
			</header><!-- .page-header -->

				<div class="page-content">
					<p><?php echo __( 'Impossible de trouver la page demandée. Peut-être aurez-vous plus de chance en tapant votre recherche dans le champ suivant', 'tbc_theme' ) ?></p>

					<?php
						get_search_form();


					?>

				</div><!-- .page-content -->
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</section><!-- #primary -->

<?php
get_sidebar();
get_footer();
