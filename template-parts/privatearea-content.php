<?php
/**
 * Template part
/*
 *
 */

$current_user = wp_get_current_user();
$is_customer  = in_array('customer', (array) $current_user->roles);
$is_member    = in_array('subscriber', (array) $current_user->roles);
$is_admin     = in_array('administrator', (array) $current_user->roles);

/*
$theme_locations = get_nav_menu_locations();
//get current page URL
$uri = $_SERVER['REQUEST_URI'];
foreach ($theme_locations as $key => $value) {

//get menu items from ith menu in loop
$items = wp_get_nav_menu_items($value);

//loop through each item in menu to check for
foreach ($items as $item) {
if (strpos($item->url, $uri) !== false) {

$menuslug = $menu->slug;
var_dump($key);

break;
}
}

}

// get array of nav menus
$menus = wp_get_nav_menus();

//get current page URL
$uri = $_SERVER['REQUEST_URI'];

//loop through each menu looking for current page
foreach ($menus as $menu) {

//get menu items from ith menu in loop
$items = wp_get_nav_menu_items($menu->term_id);

//loop through each item in menu to check for
foreach ($items as $item) {
if (strpos($item->url, $uri) !== false) {

$menuslug = $menu->slug;
break;
}
}
}
 */
?>



<!-- ============================================================== -->
<!-- Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
	<!-- Sidebar navigation-->
	<nav class="sidebar-nav">


	    <?php
if ($is_customer) {
    wp_nav_menu(array(
        'theme_location'  => 'customer_menu',
        'container'       => 'ul',
        'container_id'    => 'sidebarnav',
        'container_class' => '',
        //           'menu_id'         => false,
        'menu_class'      => 'cmenu navbar-nav',
        'depth'           => 1,
        //    'fallback_cb'        =>'wp_bootstrap_navwalker::fallback',
        'walker'          => new wp_bootstrap_navwalker(),
    ));
}
?>

	    <?php
if ($is_admin) {
    wp_nav_menu(array(
        'theme_location'  => 'admin_menu',
        'container'       => 'ul',
        'container_id'    => 'sidebarnav',
        'container_class' => '',
        //           'menu_id'         => false,
        'menu_class'      => 'cmenu navbar-nav',
        'depth'           => 1,
        //    'fallback_cb'        =>'wp_bootstrap_navwalker::fallback',
        'walker'          => new wp_bootstrap_navwalker(),
    ));
}

if ($is_member) {
    wp_nav_menu(array(
        'theme_location'  => 'member_menu',
        'container'       => 'ul',
        'container_id'    => 'sidebarnav',
        'container_class' => '',
        //           'menu_id'         => false,
        'menu_class'      => 'cmenu navbar-nav',
        'depth'           => 1,
        //    'fallback_cb'        =>'wp_bootstrap_navwalker::fallback',
        'walker'          => new wp_bootstrap_navwalker(),
    ));
}
?>
	    <!----    <ul id="sidebarnav">
<li> <a class="waves-effect waves-dark" href="index.html" aria-expanded="false"><i class="mdi mdi-gauge"></i><span class="hide-menu">Dashboard</span></a>
		</li>
		<li> <a class="waves-effect waves-dark" href="pages-profile.html" aria-expanded="false"><i class="mdi mdi-account-check"></i><span class="hide-menu">Profile</span></a>
		</li>
		<li> <a class="waves-effect waves-dark" href="table-basic.html" aria-expanded="false"><i class="mdi mdi-table"></i><span class="hide-menu">Basic Table</span></a>
		</li>
		<li> <a class="waves-effect waves-dark" href="icon-material.html" aria-expanded="false"><i class="mdi mdi-emoticon"></i><span class="hide-menu">Icons</span></a>
		</li>
		<li> <a class="waves-effect waves-dark" href="map-google.html" aria-expanded="false"><i class="mdi mdi-earth"></i><span class="hide-menu">Google Map</span></a>
		</li>
		<li> <a class="waves-effect waves-dark" href="pages-blank.html" aria-expanded="false"><i class="mdi mdi-book-open-variant"></i><span class="hide-menu">Blank Page</span></a>
		</li>
		<li> <a class="waves-effect waves-dark" href="pages-error-404.html" aria-expanded="false"><i class="mdi mdi-help-circle"></i><span class="hide-menu">Error 404</span></a>
	    </li>  </ul>--->

		<?php /*  <div class="text-center m-t-30">
<a href="https://wrappixel.com/templates/materialpro/" class="btn waves-effect waves-light btn-warning hidden-md-down"> Upgrade to Pro</a>
</div> */?>
	</nav>
	<!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
    <!-- Bottom points-->
    <div class="sidebar-footer">
	<!--<a href="" class="link" data-toggle="tooltip" title="Settings"><i class="ti-settings"></i></a>--->
	<!--<a href="" class="link" data-toggle="tooltip" title="Email"><i class="mdi mdi-gmail"></i></a>--->
	<!-- item--><a href="<?php echo wp_logout_url(home_url()); ?>" class="link" data-toggle="tooltip" title="<?php echo __('Se déconnecter', 'theme'); ?>"><i class="mdi mdi-power"></i></a> </div>
    <!-- End Bottom points-->
</aside>

<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
	<!-- ============================================================== -->
	<!-- Bread crumb and right sidebar toggle -->
	<!-- ============================================================== -->
	<div class="row page-titles">
	    <div class="col-md-8 col-8 align-self-center">
		<h3 class="text-themecolor"><?php
the_title('<h1 class="entry-title">', '</h1>');
?>
		</h3>
		<!---<ol class="breadcrumb">
		    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
		    <li class="breadcrumb-item active">Dashboard</li>
		</ol>--->




	    </div>
	    <div class="col-md-7 col-4 align-self-center">
		<?php /* <a href="https://wrappixel.com/templates/materialpro/" class="btn waves-effect waves-light btn-danger pull-right hidden-sm-down"> Upgrade to Pro</a>
 */?>
	    </div>
	</div>
	<!-- ============================================================== -->
	<!-- End Bread crumb and right sidebar toggle -->
	<!-- ============================================================== -->
	<!-- ============================================================== -->
	<!-- Start Page Content -->
	<!-- ============================================================== -->
	<!-- Row -->

	<?php

if (!in_array("administrator", $current_user->roles)) {
    ?>


	<div class="row">
	    <div class="col-12">
        		<div class="card  alert alert-warning   alert-dismissible fade show">

    		    <div class="card-block">
    			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
    			    <span aria-hidden="true">&times;</span>
    			</button>
			    <?php dynamic_sidebar('dashboard');?>
		    </div>
    		</div>
    	    </div>
    	</div>
	    <?php
}
?>

	<?php
if ($iscustomer) {
    get_template_part('template-parts/privatearea', 'contentcustomer');
} else {
    get_template_part('template-parts/privatearea', 'contentmember');
}
?>

    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->

</div>




