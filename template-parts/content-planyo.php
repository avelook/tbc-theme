<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */
get_header();
?>


<section id="primary" class="content-area planyo">
    <main id="main" class="site-main" role="main">
		<div class="container">
<?php
while ( have_posts() ) : the_post();
	?>
				<h1>
				<?php the_title(); ?>
				</h1>
					<?php
					get_template_part( 'template-parts/content', 'page' );

					$attrs = "";
					if ( isset( $_GET['mode'] ) ) {
						$attrs.= "mode=" . $_GET['mode'];
					}

					if ( isset( $_GET['ppp_resfilter'] ) ) {
						$attrs.= "&ppp_resfilter=" . $_GET['ppp_resfilter'];
					}

					if ( isset( $_GET['sort'] ) ) {
						$attrs.= "&sort=" . $_GET['sort'];
					}

					echo do_shortcode( "[planyo attribute_string='" . $attrs . "'] " );

				endwhile; // End of the loop.
				?>
		</div>
	</main><!-- #main -->
</section><!-- #primary -->

<?php
//get_sidebar();
get_footer();
