<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>


	<h2>
		<?php
				the_title(); ?></h2>
	<?php
	the_excerpt();
	//the_content();
?>

	<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php echo __( 'Lire la suite', 'tbc_theme' ); ?></a>
	</br></br>

</article><!-- #post-## -->
