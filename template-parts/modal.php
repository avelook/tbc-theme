<?php ?>
<div class="modal fade" id="loginBox" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content text-center ">
			<div class="modal-header ">
				<?php if (is_user_logged_in()) {
    ?>
<!--
						<h3 class="modal-title"><?php echo __('Espace membre', 'tbc_theme'); ?></h3> -->

					<?php } else {
    ?>

						<h3><?php //echo __( 'Connexion', 'tbc_theme' );  ?></h3>

				<?php }?>
				<button type="button" class="close" data-dismiss="modal">&times;</button>

			</div>
			<div class="modal-body">
				<p class="text-center">
					<img width="200" src="<?php echo get_theme_mod("logo"); ?>" alt="<?php echo esc_attr(get_bloginfo('name')); ?>">
				</p>
				<?php
if (is_user_logged_in()) {

    $current_user = wp_get_current_user();
    if (in_array("customer", $current_user->roles)) {

        ?>

						<p><a class="btn btn-primary btn-wp" href="<?php echo site_url() . '/espace-client'; ?>"><?php echo __('Voir votre espace client', 'tbc_theme'); ?></a></p>

						<?php
} else {
        ?>
									<p><a class="btn btn-primary btn-wp" href="<?php echo get_permalink(93); ?>"><?php echo __('Voir vos demandes de contact', 'tbc_theme'); ?></a></p>


<?php

    }

    ?>


							<p><a class="btn btn-light btn-wp" href="<?php echo wp_logout_url(home_url()); ?>"><?php echo __('Déconnexion', 'tbc_theme'); ?></a></p>




	<?php
} else {

    get_template_part('ajax', 'auth');
    ?>
							<div id = "loginBox" >
									<form id="login" class="ajax-auth" action="login" method="post">
							<?php wp_nonce_field('ajax-login-nonce', 'security');?>
								<div class="form-group">
									<input class="form-control" placeholder="<?php echo __('Utilisateur', 'tbc_theme'); ?>" id="username" type="text" class="required" name="username">
										<input class="form-control" placeholder="<?php echo __('Mot de passe', 'tbc_theme'); ?>" id="password" type="password" class="required" name="password">
									</div>

		<div class="ajax-button">
<div class="fa fa-check done"></div>
<div class="fa fa-close failed"></div>
		<button class="loginsubmit btn btn-block btn-primary" type="submit" >
		<span class="submitext"><?php echo __('Connexion', 'tbc_theme'); ?></span><span class="loading" style="display:none"></span></button>

</div>




									<p class="status"></p>
									<a class="text-link" href="<?php echo wp_lostpassword_url(); ?>"><?php echo __('Mot de passe oublié ?', 'tbc_theme'); ?></a>

					</form>
			</div>
			<?php }?>
		</div>
		</div>

	</div>

</div>


<div id="DynamicModal" class="modal  fade" role="dialog">
    <div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body">

			</div>

		</div>

	</div>
</div>