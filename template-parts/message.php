<?php
if ( filter_input( INPUT_GET, 'msg' ) ) {
	?>
	<?php
	if ( filter_input( INPUT_GET, 'msg' ) === 'no-center' ) {
		$class = "warning";
		?>
				<div class="alert alert-<?php echo $class; ?>"><?php echo __( 'Votre compte n\'a pas été associé à un centre. Vous n\'avez donc pas accès aux demandes de contact. Merci de prendre contact avec les administrateurs du site', 'tbc_theme' ); ?></div>
		</div>
			<?php
			}
			if ( filter_input( INPUT_GET, 'msg' ) === 'no-access-lead' ) {
				$class = "danger";
				?>
				<div class="alert alert-<?php echo $class; ?>"><?php echo __( 'Vous n\'avez plus accès à ce lead. Le délai de traitement est écoulé. Merci de contacter nos équipes', 'tbc_theme' ); ?></div>
						</div>
								<?php
							}
							if ( filter_input( INPUT_GET, 'msg' ) === 'no-access' ) {
								$class = "danger";
								?>
								<div class="alert alert-<?php echo $class; ?>"><?php echo __( 'Vous n\'avez pas accès à cette page', 'tbc_theme' ); ?></div>
						</div>
					<?php } ?>



	<?php
}
?>