<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

?>

<article id="post-<?php the_ID();?>" <?php post_class();?>>
	<?php
$enable_vc = get_post_meta(get_the_ID(), '_wpb_vc_js_status', true);

$template_id = get_post_meta(get_the_ID(), '_views_template', true);

$image = get_the_post_thumbnail_url(get_the_ID(), 'full');
if ($image) {
    $background = 'background-image: url(' . $image . ') !important';
} else {
    //$background = 'background-image: url(' . get_template_directory_uri() . '/assets/images/bg-page-1.jpg) !important';
    $background = 'background-image: url(' . get_template_directory_uri() . '/assets/images/bg-header-inner.jpg) !important';
}

if (is_front_page() == false && $template_id != 576) {

    ?>
	<header class="banner inner entry-header" style="<?php echo $background; ?>">
		<div class="container">
			<?php
the_title('<h1 class="entry-title">', '</h1>');
    ?>
		</div>
	</header><!-- .entry-header -->



	<?php }?>

	<div class="entry-content <?php

if (is_front_page() == false && in_array($template_id, [270])) {
    echo "inpage";
}?>">
<?php
if (is_front_page() == false && !in_array($template_id, [69, 576])) {
    ?>
				<div class="container maincontent">

				<?php
}

if (in_category('actualites')) {
    the_breadcrumb();
}

the_content();

wp_link_pages(array(
    'before' => '<div class="page-links">' . esc_html__('Pages:', 'wp-bootstrap-starter'),
    'after'  => '</div>',
));

if (is_front_page() == false && $template_id != 69) {
    ?>
				</div>
			<?php }
?>
			</div><!-- .entry-content -->

	<?php if (get_edit_post_link() && !$enable_vc): ?>
		<footer class="entry-footer">
			<?php
edit_post_link(
    sprintf(
        /* translators: %s: Name of current post */
        esc_html__('Edit %s', 'wp-bootstrap-starter'),
        the_title('<span class="screen-reader-text">"', '"</span>', false)
    ),
    '<span class="edit-link">',
    '</span>'
);
?>
		</footer><!-- .entry-footer -->
	<?php endif;?>
</article><!-- #post-## -->
