<?php
/*
 * See https://wrappixel.com/templates/materialpro-lite
 */

/* user details */
$current_user	=wp_get_current_user();
$user_long	=$current_user->user_firstname.' '.$current_user->user_lastname;//$current_user->user_email;
$user_avatar	=get_avatar($current_user->ID,100);
?>

<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>



<!-- ============================================================== -->
<!-- Preloader - style you can find in spinners.css -->
<!-- ============================================================== -->
<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
    <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
</div>



<header class="topbar">
    <nav class="navbar top-navbar navbar-toggleable-sm navbar-light">
	<!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="index.html">
                        <!-- Logo icon -->
                            <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->

                            <!-- Light Logo icon -->
			    <span class="sitename">    <?php echo get_bloginfo();?></span>
			    <?php /* <img src="<?php echo get_theme_mod('logo');?>" width="35" alt="<?php echo esc_attr(get_bloginfo('name'));?>"> */?>

			    <!--End Logo icon -->
                        <!-- Logo text --><span>

                         <!-- Light Logo text -->

			 </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto mt-md-0">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>
                        <!-- ============================================================== -->
                        <!-- Search -->
                        <!-- ============================================================== -->
			<?php /* <li class="nav-item hidden-sm-down search-box"> <a class="nav-link hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-search"></i></a>
			  <form class="app-search">
			  <input type="text" class="form-control" placeholder="Search &amp; enter"> <a class="srh-btn"><i class="ti-close"></i></a> </form>
			  </li> */?>
                    </ul>
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav my-lg-0">
                        <!-- ============================================================== -->
                        <!-- Profile -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				<span class="profile-pic m-r-10"><?php echo $user_avatar;?></span><?php echo $user_long;?>

			    </a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>


<?php /*

  <header id="masthead" class="col-md-12 col-lg-12 col-sm-12" role="banner">

  <nav class="navbar  navbar-light">
  <a class="navbar-brand"  href="<?php echo site_url();?>">
  <img src="<?php echo get_theme_mod('logo');?>" alt="<?php echo esc_attr(get_bloginfo('name'));?>">
  </a>

  <div  class="d-flex ">
  <div>
  <?php echo $user_long;?>
  <br><a class="" href="<?php echo wp_logout_url(home_url());?>"><?php echo __('Déconnexion','theme');?></a>
  </div>
  <div id="avatar">
  <?php echo $user_avatar;?>
  </div>
  </div>

  </nav>



  </header><!-- #masthead -->

 */?>


