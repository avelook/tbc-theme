<div class="row">
    <div class="col-lg-8 col-md-7">
	<div class="card">
	    <div class="card-block">
		<div class="row">
		    <div class="col-12">
			<div class="">
			    <div>

				<?php
				the_content();
				?>

			    </div>
			</div>
			<div class="col-12">
			    <div class="amp-pxl" style="height: 360px;"></div>
			</div>
		    </div>
		</div>
	    </div>

	</div>

	<!----
	<div class="card">
	    <div class="card-block">
		<div class="row">
		    <div class="col-12">
			<div class="d-flex flex-wrap">
			    <div>
				<h3 class="card-title">Sales Overview</h3>
				<h6 class="card-subtitle">Ample Admin Vs Pixel Admin</h6> </div>
			    <div class="ml-auto">
				<ul class="list-inline">
				    <li>
					<h6 class="text-muted text-success"><i class="fa fa-circle font-10 m-r-10 "></i>Ample</h6> </li>
				    <li>
					<h6 class="text-muted  text-info"><i class="fa fa-circle font-10 m-r-10"></i>Pixel</h6> </li>
				</ul>
			    </div>
			</div>
		    </div>
		    <div class="col-12">
			<div class="amp-pxl" style="height: 360px;"></div>
		    </div>
		</div>
	    </div>
	</div>
--->
    </div>


    <div class="col-lg-4 col-md-5">
	<div class="card">
	    <div class="card-block">
		<h2 class="card-title">Nos actualités</h2>
		<h6 class="card-subtitle">Toutes les alertes et dernières nouvelles</h6>
		<div id="visitor" style="height:290px; width:100%;">

		    <?php echo do_shortcode('[centers_related_news]');?></div>
	    </div>
	    <div>
		<hr class="m-t-0 m-b-0">
	    </div>
	    <div class="card-block text-center ">
		<a href="" class="btn waves-effect waves-light btn-danger pull-right hidden-sm-down">Nous écrire</a>

	    </div>
	</div>
    </div>

</div>