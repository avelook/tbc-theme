<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WP_Bootstrap_Starter
 */

/* <a href="https://www.facebook.com/teambusinesscenters/" target="_blank"><img width="40" src="' . get_template_directory_uri() . '/assets/images/pic-fb.png"/></a>
<a href="https://www.linkedin.com/company/team-business-centers/" target="_blank"><img width="40"  src="' . get_template_directory_uri() . '/assets/images/pic-linkedin.png"/></a>
<a href="https://plus.google.com/b/118001321152710800381/discover" target="_blank" ><img width="40"  src="' .  get_template_directory_uri() . '/assets/images/pic-gplus.png"/></a>
 */
?>




<?php if (!is_page_template('blank-page.php') && !is_page_template('blank-page-with-container.php')): ?>
		<!-- </div>.row -->
	<!-- 	</div>.container -->
		</div><!-- #content -->


    		<footer id="colophon" class="site-footer" role="contentinfo">

				<div id="prefooter" class="bggrey">
					<div class="container">
							<?php if (is_home() || is_front_page()): ?>
								<div id="prefooter1" class="d-flex p-4 justify-content-center">



								    <div id="social">
										<?php dynamic_sidebar('footersocial');?>
									    </div>
									    <div id="linkth">
										    <a rel="#prefooter2" class=" linkm togglediv"><?php echo __('En savoir plus sur', 'tbc_theme'); ?> <?php bloginfo();?></a>
									</div>



								</div>

								<div id="prefooter2" class="">


						<div class="row pt-4 pb-4">
							<div class="col-md-9">
									<?php dynamic_sidebar('footer-1');?>

								</div>
							<div class="col-md-3">
									<a><img  src="<?php echo get_template_directory_uri(); ?>/assets/images/logo-legalvision.jpg"/></a>
															</div>
											</div>
					</div>

						<?php endif;?>

				</div>
			</div>

				<div id="footer" class="small-font bgmediumgrey" >
					<div class="container">
							<?php

if (is_front_page() == false):
?>
		<?php echo $social; ?>
							<?php endif;?>


							<?php
dynamic_sidebar('footer-2');
?>

					</div>
					</div>



	</footer><!-- #colophon -->
	<div id="contactbtn_wrapper"><a id="contactbtn" href="#sb_intro" class="page-scroller hidden"><?php echo __('Contactez le(s) centre(s) sélectionné(s)', 'tbc_theme'); ?></a></div>



<?php endif;?>

		</div><!-- #page -->


				<?php wp_footer();?>




		<?php echo get_theme_mod('ga_code'); ?>


		<!-- MODAL --->
		<?php require_once get_template_directory() . '/template-parts/modal.php';?>



		<!----
<script type="text/javascript">

var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
try{
var pageTracker = _gat._getTracker("UA-39827740-1");
pageTracker._trackPageview();
} catch(err) {}</script>
---->

		<script>

		var theme=
		  {
			  base_url: "<?php echo get_template_directory_uri(); ?>"

		};
		</script>
		</body>
		</html>
