<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WP_Bootstrap_Starter
 */
trigger_error( "Task done at " . strftime( '%H:%m:%S', time() ), E_USER_NOTICE );
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="x-ua-compatible" content="ie=edge"> 
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>

	<script>
	//Redirect if IE9 and less
	if (navigator.appVersion.indexOf("MSIE 9") !== -1)
	{
		window.location("http://windows.microsoft.com/en-us/internet-explorer/download-ie");
	}
	</script>


<header>

    <div id="stickymsgbox" class="hidden"><div></div></div>

</header>




<body <?php body_class(); ?>>
    <div id="page" class="site">


	<header id="masthead" class="fixed-top col-md-12 col-lg-12 col-sm-12" role="banner">

    	    <nav class="navbar  navbar-light">
		<a class="navbar-brand"  href="<?php echo home_url();?>">
    		    <img src="<?php echo get_theme_mod('logo');?>" alt="<?php echo esc_attr(get_bloginfo('name'));?>">
    		</a>

    		<div id="nav-mobile" class="d-block d-xxl-none">
    		    <span id="tel_tbc" class="font-weight-bold"><span class="border-bottom "><?php echo get_theme_mod('phone');?></span>

    		    </span>
    		    <button class="navbar-toggler btn_toggler " type="button"  data-target="#mobilemenu">

    			<span class="title_menu"><?php echo __('Menu','tbc_theme');?></span><span class="navbar-toggler-icon"></span>
    		    </button>
    		</div>

    		<div id="mobilemenu" class="d-none d-xxl-none">
			<?php
			wp_nav_menu(array (
			    'theme_location'	=>'mainmenu',
			    'container'		=>'ul',
			    'container_id'		=>'',
			    'container_class'	=>'',
			    //           'menu_id'         => false,
			    'menu_class'		=>'navbar-nav row d-flex justify-content-left',
			    'depth'			=>2,
			    //    'fallback_cb'		=>'wp_bootstrap_navwalker::fallback',
			    'walker'		=>new wp_bootstrap_navwalker()
			));
			?>
    		</div>

    		<div id="mainmenu" class="col d-none d-xxl-block ">

			<?php
			wp_nav_menu(array (
			    'theme_location'	=>'mainmenu',
			    'container'		=>'ul',
			    'container_id'		=>'',
			    'container_class'	=>'',
			    //           'menu_id'         => false,
			    'menu_class'		=>'navbar-nav col d-flex justify-content-left',
			    'depth'			=>1,
			    //    'fallback_cb'		=>'wp_bootstrap_navwalker::fallback',
			    'walker'		=>new wp_bootstrap_navwalker()
			));
			?>




    		</div>

		    <?php
		    get_template_part('template-parts/extra-masthead');
		    ?>

    	    </nav>

    	</header><!-- #masthead -->




	    <?php
	    wp_nav_menu(array (
		'theme_location'	=>'mainmenu',
    'container'		=>'nav',
		'items_wrap'		=>'%3$s',
		'container_id'		=>'submenus',
		'container_class'	=>'d-none d-xxl-block',
		'walker'		=>new wp_bootstrap_navwalker_submenus()
	    ));




	    get_template_part( 'template-parts/message' );
				?>


		<div id="content" class="site-content">
			<!-- <div class="container">-->
				<!--	<div class="row">-->
