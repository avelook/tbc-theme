<?php

/**
 * Contains methods for customizing the theme customization screen.
 *
 * @link http://codex.wordpress.org/Theme_Customization_API
 * @since MyTheme 1.0
 */
class AvelookTheme_Customize
{

    /**
     * This hooks into 'customize_register' (available as of WP 3.4) and allows
     * you to add new sections and controls to the Theme Customize screen.
     *
     * Note: To enable instant preview, we have to actually write a bit of custom
     * javascript. See live_preview() for more.
     *
     * @see add_action('customize_register',$func)
     * @param \WP_Customize_Manager $wp_customize
     * @link http://ottopress.com/2012/how-to-leverage-the-theme-customizer-in-your-own-themes/
     * @since MyTheme 1.0
     */
    public static function register( $wp_customize )
    {
	//1. Define a new section (if desired) to the Theme Customizer
	$wp_customize->add_section('mytheme_options',array (
	    'title' => __( 'Options', 'theme' ), //Visible title of section
			'priority'	=>35,//Determines what order this appears in
	    'capability'	=>'edit_theme_options',//Capability needed to tweak
	    'description' => __( 'Options du thème', 'theme' ), //Descriptive tooltip
				)
	);

	//2. Register new settings to the WP database...
	$wp_customize->add_setting('logo',//No need to use a SERIALIZED name, as `theme_mod` settings already live under one db record
			    array (
	    'default'	=>'',//Default setting/value to save
	    'type'		=>'theme_mod',//Is this an 'option' or a 'theme_mod'?
	    'capability'	=>'edit_theme_options',//Optional. Special permissions for accessing this setting.
	    'transport'	=>'postMessage',//What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
	)
	);

	//3. Finally, we define the control itself (which links a setting to a section and renders the HTML controls)...
	$wp_customize->add_control(
	new WP_Customize_Upload_Control(
	$wp_customize,'mytheme_logo',array (
	    'label' => __( 'Logo du site', 'theme' ),
			'section'	=>'mytheme_options',
	    'settings'	=>'logo',
	))
	);

	$wp_customize->add_setting('bg-login',//No need to use a SERIALIZED name, as `theme_mod` settings already live under one db record
			    array (
	    'default'	=>'',//Default setting/value to save
	    'type'		=>'theme_mod',//Is this an 'option' or a 'theme_mod'?
	    'capability'	=>'edit_theme_options',//Optional. Special permissions for accessing this setting.
	    'transport'	=>'postMessage',//What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
	)
	);

	$wp_customize->add_control(
	new WP_Customize_Upload_Control(
	$wp_customize,'mytheme_bg-login',array (
	    'label' => __( 'Background page de connexion', 'theme' ),
			'section'	=>'mytheme_options',
	    'settings'	=>'bg-login',
	))
	);

		$wp_customize->add_setting( 'phone', //No need to use a SERIALIZED name, as `theme_mod` settings already live under one db record
				array(
			'default' => '', //Default setting/value to save
			'type' => 'theme_mod', //Is this an 'option' or a 'theme_mod'?
			'capability' => 'edit_theme_options', //Optional. Special permissions for accessing this setting.
			'transport' => 'postMessage', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
				)
		);

		$wp_customize->add_control(
			 'phone', array(
			'type' => 'text',
			'section' => 'mytheme_options', // Add a default or your own section
			'label' => __( 'Téléphone', 'theme' ),
				)
		);


		$wp_customize->add_setting('ga_code',//No need to use a SERIALIZED name, as `theme_mod` settings already live under one db record
			    array (
	    'default'	=>'',//Default setting/value to save
	    'type'		=>'theme_mod',//Is this an 'option' or a 'theme_mod'?
	    'capability'	=>'edit_theme_options',//Optional. Special permissions for accessing this setting.
	    'transport'	=>'postMessage',//What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
	)
	);

	$wp_customize->add_control(
	'ga_code',array (
	    'type'		=>'textarea',
	    'section'	=>'mytheme_options',// Add a default or your own section
	    'label'		=>__('Code Google Analytics','theme'),
	)
	);



	//4. We can also change built-in settings by modifying properties. For instance, let's make some stuff use live preview JS...
	/* $wp_customize->get_setting('blogname')->transport		='postMessage';
	  $wp_customize->get_setting('blogdescription')->transport	='postMessage';
	  $wp_customize->get_setting('header_textcolor')->transport	='postMessage';
	  $wp_customize->get_setting('background_color')->transport	='postMessage'; */
    }
    /**
     * This will output the custom WordPress settings to the live theme's WP head.
     *
     * Used by hook: 'wp_head'
     *
     * @see add_action('wp_head',$func)
     * @since MyTheme 1.0
     */
    public static function header_output()
    {
	?>
	<!--Customizer CSS-->
	<style type="text/css">

		</style>
	<!--/Customizer CSS-->
	<?php
    }
    /**
     * This outputs the javascript needed to automate the live settings preview.
     * Also keep in mind that this function isn't necessary unless your settings
     * are using 'transport'=>'postMessage' instead of the default 'transport'
     * => 'refresh'
     *
     * Used by hook: 'customize_preview_init'
     *
     * @see add_action('customize_preview_init',$func)
     * @since MyTheme 1.0
     */
    public static function live_preview()
    {
	wp_enqueue_script(
	'mytheme-themecustomizer',// Give the script a unique ID
 get_template_directory_uri().'/assets/js/theme-customizer.js',// Define the path to the JS file
 array ('jquery','customize-preview'),// Define dependencies
 '',// Define a version (optional)
 true // Specify whether to put in footer (leave this true)
	);
    }
    /**
     * This will generate a line of CSS for use in header output. If the setting
     * ($mod_name) has no defined value, the CSS will not be output.
     *
     * @uses get_theme_mod()
     * @param string $selector CSS selector
     * @param string $style The name of the CSS *property* to modify
     * @param string $mod_name The name of the 'theme_mod' option to fetch
     * @param string $prefix Optional. Anything that needs to be output before the CSS property
     * @param string $postfix Optional. Anything that needs to be output after the CSS property
     * @param bool $echo Optional. Whether to print directly to the page (default: true).
     * @return string Returns a single line of CSS with selectors and a property.
     * @since MyTheme 1.0
     */
    public static function generate_css( $selector, $style, $mod_name,
					 $prefix='', $postfix='', $echo=true )
    {
		
	$return	='';
	$mod	=get_theme_mod($mod_name);


	if (!empty($mod))
	{
	    $return=sprintf('%s { %s:%s; }',$selector,$style,$prefix.$mod.$postfix
	    );
	    if ($echo)
	    {
		echo $return;
	    }
	}
	return $return;
    }

}

// Setup the Theme Customizer settings and controls...
add_action('customize_register',array ('AvelookTheme_Customize','register'));

// Output custom CSS to live site
add_action('wp_head',array ('AvelookTheme_Customize','header_output'));

// Enqueue live preview javascript in Theme Customizer admin screen
add_action('customize_preview_init',array ('AvelookTheme_Customize','live_preview'));

