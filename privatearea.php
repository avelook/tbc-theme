<?php
/**
 * Template Name: Private Area
 * See https://wrappixel.com/demos/free-admin-templates/material-pro-lite/html/
 */

?><!DOCTYPE html>
<html <?php language_attributes();?>>
<head>
	<meta charset="<?php bloginfo('charset');?>">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head();?>

	<script>
	//Redirect if IE9 and less
	if (navigator.appVersion.indexOf( "MSIE 9" ) !== -1 )
	    {
		window.location( "http://windows.microsoft.com/en-us/internet-explorer/download-ie" );
	    }
	</script>



</head>
<body <?php body_class();?>>
    <div id="page" class="pwa privatearea fix-header fix-sidebar card-no-border">

	<?php
get_template_part('template-parts/privatearea', 'header');
?>

	<section id="primary" class="">
	    <main id="main" class="site-main" role="main">

	<?php
while (have_posts()): the_post();

    get_template_part('template-parts/privatearea', 'content');

endwhile; // End of the loop.
?>

    </main><!-- #main -->
</section><!-- #primary -->

    </div>

<!-- GA CODE --->
<?php //echo get_theme_mod('ga_code');?>


<!-- MODAL --->
<?php require_once get_template_directory() . '/template-parts/modal.php';?>


<?php wp_footer();?>


    </body>
    </html>