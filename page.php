<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

get_header();


if ( in_array( 69, get_post_meta( get_the_ID() ) ['_views_template'] ) ):$class = "pt-0";
endif;
?>


<section id="primary" class="content-area ">
    <main id="main" class="site-main <?php echo $class; ?>" role="main">


		<?php
	
		//	the_breadcrumb();
		?>

	<?php



			while (have_posts()) : the_post();



	
		get_template_part('template-parts/content','page');



			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</section><!-- #primary -->

<?php
                        //get_sidebar();
get_footer();
