/**
 * iosCheckbox.js
 * Version: 1.0.0
 * Author: Ron Masas
 */

(function ($) {
    $.fn.extend({
        iosCheckbox: function ( ) {
            $(this).each(function () {
                /**
                 * Original checkbox element
                 */
                var org_checkbox = $(this);
                /**
                 * iOS checkbox div
                 */
                var ios_checkbox = jQuery("<div>", {class: 'ios-ui-select'}).append(jQuery("<div>", {class: 'inner'}));

                var wrapper = $(this).parents('.centerselect');
                // If the original checkbox is checked, add checked class to the ios checkbox.
                if (org_checkbox.is(":checked")) {
                    wrapper.addClass("checked");
                }
                // Hide the original checkbox and print the new one.
                org_checkbox.hide().after(ios_checkbox);
                // Add click event listener to the ios checkbox
                wrapper.click(function () {
                    // Toggel the check state
                    wrapper.toggleClass("checked");
                    // Check if the ios checkbox is checked
                    if (wrapper.hasClass("checked")) {
                        // Update state
                        org_checkbox.prop('checked', true);
                    } else {
                        // Update state
                        org_checkbox.prop('checked', false);
                    }
                });
            });
        }
    });
})(jQuery);
