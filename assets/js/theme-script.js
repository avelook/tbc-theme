/*
 *
 * MAIN THEME JS TBC
 */

jQuery(function ($) {


    jQuery(document).on('vc-full-width-row', function () {
        var fullrow = jQuery('[data-vc-full-width="true"]');
        fullrow.css("opacity", 1);
    });


    /*
     *
     * DROPDOWN MENU
     *  */
    'use strict';
    $('ul.dropdown-menu [data-toggle=dropdown]').on('click', function (event) {
        event.preventDefault();
        event.stopPropagation();
        $(this).parent().siblings().removeClass('open');
        $(this).parent().toggleClass('open');


    });
    /*
     * Animation home banner on init
     *
     */

    jQuery("#homebanner").addClass("loaded");
    /*
     * Animation home stats
     */


    jQuery(".stat_box").viewportChecker({
        callbackFunction: function ()
        {
            jQuery(".statitem0 .stat_value").countTo({
                from: 0,
                to: jQuery(".statitem0 .stat_value").html(),
                speed: 2000,
                refreshInterval: 500,
                onComplete: function (value) {
                }
            });
            jQuery(".statitem1 .stat_value").countTo({
                from: 0,
                to: jQuery(".statitem1 .stat_value").html(),
                speed: 2000,
                refreshInterval: 500,
                onComplete: function (value) {
                }
            });
            jQuery(".statitem2 .stat_value").countTo({
                from: 0,
                to: jQuery(".statitem2 .stat_value").html(),
                speed: 2000,
                refreshInterval: 100,
                onComplete: function (value) {
                }
            });
            jQuery(".statitem3 .stat_value").countTo({
                from: 0,
                to: jQuery(".statitem3 .stat_value").html(),
                speed: 1500,
                refreshInterval: 50,
                onComplete: function (value) {
                }
            });
        }
    });
    /*
     *
     * SMOOTH SCROLL
     *  */
    $('.page-scroller').on('click', function (e) {
        e.preventDefault();
        var target = this.hash;
        var $target = $(target);
        $('html, body').animate({
            'scrollTop': $target.offset().top
        }, 1000, 'swing');
    });


    
     /*
     *
     * SUBMENU MAINMENU
     */
    $('#mainmenu li a.dropdown-toggle.nav-link').click(function (e) {

        //close other submenus
        var others = $(this).parents("li").siblings();


        others.each(function ()
        {
            $(this).removeClass('open');
            $("#" + $(this).find(".dropdown-toggle").attr("rel")).removeClass('open');
        })


        //toggle this
        jQuery(this).parents('.nav-item').toggleClass('open');
        var idmenu = $(this).attr("rel");
        var submenu = $("#" + idmenu);
        submenu.toggleClass("open");
        e.stopPropagation();
    });

    /*
     *
     * SUBMENU MOBILE
     */
    $('#mobilemenu nav li:not(".open") a.dropdown-toggle.nav-link').click(function (e) {
        /*  jQuery(this).next("ul").toggleClass("open");
         e.stopPropagation();*/
    });
    /*
     *
     * READMORE LINK
     */

    $(".readmore").on('click', function ()
    {
        $(this).next().toggleClass('d-none');
    });
    /*
     *
     * FOOTER LINK
     */
    $('.togglediv').click(function (e) {
        var element = $(this).attr("rel");
        jQuery(this).toggleClass("open");
        jQuery(element).toggleClass("open");
        e.stopPropagation();
        var $target = $(element);
        $('html, body').animate({
            'scrollTop': ($target.offset().top) - 50
        }, 1000, 'swing');
    });
    /*
     *
     * MARGIN TOP
     */
    function adjustWrapperSticky()
    {
        var mtop = $("#masthead").height();

        $("#page").css('margin-top', mtop);
                $("#mobilemenu").css('top', mtop);
    }


    adjustWrapperSticky();

    /*
     *
     * STICKY
     */
    function sticky() {
        var scrollPos = $(document).scrollTop();
        if (scrollPos > 1) {
            $('#masthead').addClass('is_sticky');
            adjustWrapperSticky();
        } else {
            $('#masthead').removeClass('is_sticky');
            adjustWrapperSticky();
        }
    }
    $(window).scroll(function () {
        sticky();
    });
    /*bg search engine banner*/




    $('a[data-toggle="pill"]').on('shown.bs.tab', function (e) {
        var bg = $(this).attr("bg-url");
        $("#homebanner").css("background-image", "url('" + bg + "')");
    })


    /*
     *
     * BTN TOGGLER MOBILE MENU
     *  */
    jQuery(".btn_toggler").on("click", function ()
    {
        var element = jQuery(this).attr("data-target");
        jQuery(this).toggleClass("target-open");
        jQuery(element).toggleClass("d-none");
    });
    /*
     * On scroll : display contact button
     */

    $(window).scroll(function () {
        displayContactBtn();
    });
    function displayContactBtn()
    {
        var scrollPos = $(document).scrollTop();
        if (scrollPos > 1) {
            $('#floatedbtncontact').addClass('d-block');
        } else
        {
            $('#floatedbtncontact').addClass('d-none');
        }
    }

    /*
     *
     * CAROUSEL
     *  */
    $('#carousel_news').owlCarousel({
        loop: true,
        margin: 10,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                nav: true
            },
            980: {
                items: 1,
                nav: false
            },
            1000: {
                items: 3,
                nav: true,
                loop: false
            }
        },
        nav: true,
        autoHeight: true,
        navText: ["<img  width=32 src='" + theme.base_url + "/assets/images/arrow-left-grey.png'>", "<img width=32  src='" + theme.base_url + "/assets/images/arrow-right-grey.png'>"]
    });
    /*item slider on page results*/

    /*slider on content page*/
    $('.slider_content').owlCarousel({
        slideSpeed: 300,
        paginationSpeed: 400,
        items: 1,
        nav: true,
        // autoHeight: true,
        navText: ["<img  width=32 src='" + theme.base_url + "/assets/images/arrow-left.png'>", "<img width=32  src='" + theme.base_url + "/assets/images/arrow-right.png'>"]
    });
    //adjust dimensions carousel
    /*  showResultsCarousel();
     $(window).on('resize', function (e) {
     showResultsCarousel();
     }).resize();*/

    /*slider on result page*/



    /*
     $(document).on('js_event_wpv_pagination_completed', function (event, data) {
     searchpage.init();
     });


     $(document).on('js_event_wpv_parametric_search_results_updated', function (event, data) {
     searchpage.init();
     });
     */




    $(window).on('resize', function (e) {
        adjustOwlDimensions();
        
adjustWrapperSticky();
        /*   resultsowl.owlCarousel({
         slideSpeed: 300,
         paginationSpeed: 400,
         items: 1,
         responsiveClass: true,
         nav: true,
         navText: ["<img  width=32 src='" + theme.base_url + "/assets/images/arrow-left.png'>", "<img width=32  src='" + theme.base_url + "/assets/images/arrow-right.png'>"]
         });*/
    }).resize();
    /*
     *
     * INIT
     * * /*/

    // localisation_select.init();

    searchpage.init();
    /*tooltip*/
    $('.hastooltip').tooltip();
    /* var test = WPViews.ViewAddonMaps();
     console.log(test.get_map_marker("49", 2));
     */

    /*search engine select on inner page*/
    $("#prod_selector select").on("change", function ()
    {
        var element = jQuery(this).find('option:selected').attr("toggle");
        jQuery("#form_wrapper > div").addClass("d-none");
        jQuery("#" + element).removeClass("d-none");
        jQuery("#prod_wrapper").removeClass();
        jQuery("#prod_wrapper").addClass(element);
    })






});
/************************************* SEARCH PAGE FUNCTIONS     ***********************************************************/

var searchpage =
        {
            init: function ()
            {


                $('.inner_sengine select,.sengine-form select').niceSelect("destroy");
                $('.inner_sengine select,.sengine-form select').niceSelect();
                localisation_select.init();
                /*iosCheckbox*/
                $(".ios").iosCheckbox();
                /*owl carousel*/
                searchpage.owl();
            },
            owl: function ()
            {

                var resultsowl = $('#results_list .slider');
                resultsowl.on('initialize.owl.carousel', function (event) {
                    adjustOwlDimensions();
                });
                resultsowl.owlCarousel({
                    slideSpeed: 300,
                    lazyLoad: true,
                    paginationSpeed: 400,
                    items: 1,
                    responsiveClass: true,
                    nav: true,
                    navText: ["<img  width=32 src='" + theme.base_url + "/assets/images/arrow-left.png'>", "<img width=32  src='" + theme.base_url + "/assets/images/arrow-right.png'>"]
                });
            }


        }

/************************************* SEARCH ENGINE MAJ CITIES DEPENDING COUNTRY     ***********************************************************/


var localisation_select = {
    init: function ()
    {

        jQuery("select[name='wpv-wpcf-pays[]'], select[name='wpcf[pays]'],select[name='wpcf-pays']").on("change", function ()
        {
            selected = jQuery(this).val();
            if (selected != '')
            {
                localisation_select.majcities(selected, null);
            }
        });
        //cred form
        jQuery("document").on("change", "select[name='wpcf-ville']", function ()
        {
            alert('ok arr');
            selected = jQuery(this).val();
            if (selected != '')
            {
                localisation_select.majArr(selected);
            }

        });
        //remove arrondissements fields when city is changed
        jQuery("select[name='wpv-wpcf-ville[]'], select[name='wpcf[ville]'],select[name='wpcf-ville']").on("change", function ()
        {
            jQuery("#wpv_control_select_wpcf-arrondissement").parents('.col').remove();
        })

        //select France as default value on home
        jQuery(".home #wpv_control_select_wpv-wpcf-pays option").removeAttr("selected");
        localisation_select.selectCountry(2, null);
        localisation_select.selectCountry(64, null);
    },
    majcities: function (country, city)
    {
        var data = {'country': country, 'action': 'getcities'};
        var city = city;
        jQuery.ajax(
                {
                    type: "POST",
                    context: this,
                    url: tbc_vars.ajaxurl,
                    data: data,
                    cache: false,
                    beforeSend: function ()
                    {
                        localisation_select.showPreloader();
                    },
                    success: function (msg)
                    {
                        localisation_select.hidePreloader();
                        jQuery("select[name='wpv-wpcf-ville[]']").html(msg);
                        jQuery("select[name='wpcf[ville]']").html(msg);
                        jQuery("select[name='wpcf-ville']").html(msg);
                        $('.inner_sengine select,.sengine-form select').niceSelect("destroy");
                        $('.inner_sengine select,.sengine-form select').niceSelect();
                        localisation_select.selectCity(city);
                        //response = JSON.stringify( msg );
                    }
                });
    },
    majArr: function (city)
    {
        var data = {'city': city, 'action': 'getarrs'};
        jQuery.ajax(
                {
                    type: "POST",
                    context: this,
                    url: tbc_vars.ajaxurl,
                    data: data,
                    cache: false,
                    beforeSend: function ()
                    {
                        select_localisation.showPreloader();
                    },
                    success: function (msg)
                    {
                        select_localisation.hidePreloader();
                        jQuery("select[name='wpcf[arrondissement]']").html(msg);
                        //    select_localisation.selectCity( city );
                        //response = JSON.stringify( msg );
                    }
                });
    },
    selectCountry: function (country, callbackfn)
    {
        jQuery(".home #wpv_control_select_wpv-wpcf-pays option[value='" + country + "']").attr("selected", "selected");
        jQuery('select').niceSelect('update');
        if (callbackfn != null)
        {
            callbackfn();
        }

    },
    selectCity: function (city)
    {
        jQuery("select[name='wpv-wpcf-ville[]'] option[value=" + city + "]").attr("selected", "selected");
        $('select').niceSelect('update');
    },
    showPreloader: function ()
    {
        var pending = "<span id='citiesloader'>loading...</span>";
        jQuery("select[name='wpv-wpcf-ville[]']").parents(".form-group").find(".nice-select .current").hide();
        jQuery("select[name='wpv-wpcf-ville[]']").parents(".form-group").find(".nice-select .current").after(pending);
    },
    hidePreloader: function ()
    {
        jQuery("#citiesloader").remove();
        jQuery(".nice-select .current").show();
    }


};
/************************************* CARROUSEL  ***********************************************************/

function adjustOwlDimensions()
{
    jQuery(".inner_slider .item").css("width", function () {
        var width = jQuery(".item-result-content").innerWidth();
        return width;
    });
    jQuery(".inner_slider .item").css("height", function () {
        height = jQuery(".item-result-content").innerHeight();
        return height;
    });
}





/************************************* TIMER     ***********************************************************/

(function ($)

{
    $.fn.countTo = function (options) {
        // merge the default plugin settings with the custom options
        options = $.extend({}, $.fn.countTo.defaults, options || {});
        // how many times to update the value, and how much to increment the value on each update
        var loops = Math.ceil(options.speed / options.refreshInterval),
                increment = (options.to - options.from) / loops;
        return $(this).each(function () {
            var _this = this,
                    loopCount = 0,
                    value = options.from,
                    interval = setInterval(updateTimer, options.refreshInterval);
            function updateTimer() {
                value += increment;
                loopCount++;
                $(_this).html(value.toFixed(options.decimals));
                if (typeof (options.onUpdate) == 'function') {
                    options.onUpdate.call(_this, value);
                }

                if (loopCount >= loops) {
                    clearInterval(interval);
                    value = options.to;
                    if (typeof (options.onComplete) == 'function') {
                        options.onComplete.call(_this, value);
                    }
                }
            }
        });
    };
    $.fn.countTo.defaults = {
        from: 0, // the number the element should start at
        to: 100, // the number the element should end at
        speed: 1000, // how long it should take to count between the target numbers
        refreshInterval: 100, // how often the element should be updated
        decimals: 0, // the number of decimal places to show
        onUpdate: null, // callback method for every time the element is updated,
        onComplete: null, // callback method for when the element finishes updating
    };
})(jQuery);
/*********************************** VIEWPORT CHECKER *********************************************/
/*
 jQuery.fn.viewportChecker = function (useroptions) {
 // Define options and extend with user
 var options = {
 classToAdd: 'visible',
 offset: 100,
 callbackFunction: function (elem) {}
 };
 jQuery.extend(options, useroptions);
 // Cache the given element and height of the browser
 var $elem = this,
 windowHeight = jQuery(window).height();
 this.checkElements = function () {
 // Set some vars to check with
 var scrollElem = ((navigator.userAgent.toLowerCase().indexOf('webkit') != -1) ? 'body' : 'html'),
 viewportTop = jQuery(scrollElem).scrollTop(),
 viewportBottom = (viewportTop + windowHeight);
 $elem.each(function () {
 var $obj = jQuery(this);
 // If class already exists; quit
 if ($obj.hasClass(options.classToAdd)) {
 return;
 }

 // define the top position of the element and include the offset which makes is appear earlier or later
 var elemTop = Math.round($obj.offset().top) + options.offset,
 elemBottom = elemTop + ($obj.height());
 // Add class if in viewport
 if ((elemTop < viewportBottom) && (elemBottom > viewportTop)) {
 $obj.addClass(options.classToAdd);
 // Do the callback function. Callback wil send the jQuery object as parameter
 options.callbackFunction($obj);
 }
 });
 };
 // Run checkelements on load and scroll
 jQuery(window).scroll(this.checkElements);
 this.checkElements();
 // On resize change the height var
 jQuery(window).resize(function (e) {
 windowHeight = e.currentTarget.innerHeight;
 });
 };

 */


