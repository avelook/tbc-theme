jQuery(document).ready(function ($) {
//
    // Perform AJAX login on form submit
    $('form#login').on('submit', function (e) {
        /*    if (!$(this).valid())
         return false;*/
        console.log(ajax_auth_object.ajaxurl);
        // $('p.status', this).show().text(ajax_auth_object.loadingmessage);
        action = 'ajaxlogin';
        username = $('form#login #username').val();
        password = $('form#login #password').val();
        email = '';
        security = $('form#login #security').val();
        if ($(this).attr('id') == 'register') {
            action = 'ajaxregister';
            username = $('#signonname').val();
            password = $('#signonpassword').val();
            email = $('#email').val();
            security = $('#signonsecurity').val();
        }
        ctrl = $(this);
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: ajax_auth_object.ajaxurl,
            data: {
                'action': action,
                'username': username,
                'password': password,
                'email': email,
                'security': security
            },
            beforeSend: function ()
            {
                $(".loading").show();
                $(".submitext").hide();
            },
            success: function (data) {
                $(".loading").hide();
                $(".submitext").show();
                $('p.status', ctrl).text(data.message);
                if (data.loggedin == true) {

                 console.log(data.roles.length);
                    for($i=0;$i<data.roles.length;$i++)
                    {
                        console.log(data.roles[$i]);
                        if(data.roles[$i]=="customer")
                        {
                             document.location.href = ajax_auth_object.redirecturl_customer;
                        }
                        else
                        {
 document.location.href = ajax_auth_object.redirecturl_member;
                        }
                    }
                   
                }
            }
        });
        e.preventDefault();
    });
    // Client side form validation
    if (jQuery("#register").length)
        jQuery("#register").validate(
                {
                    rules: {
                        password2: {equalTo: '#signonpassword'
                        }
                    }}
        );
    else if (jQuery("#login").length)
        console.log("test");
    //  jQuery("#login").validate();
});














