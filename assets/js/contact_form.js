
var contact_form = {
    'updateStates': function ()
    {

        selected = 0;
        jQuery(".centerselect").each(function ()
        {
            var rel = jQuery(this).attr("rel");
            if (jQuery(this).hasClass("checked"))
            {
                selected++;

                contact_form.update_center(rel, true);
            } else
            {
                contact_form.update_center(rel, false);
            }


        });

        if (selected > 0)
        {
            jQuery(".dyn").show();
            jQuery(".dyn .number").html(selected);
            jQuery(".ndyn").hide();

            jQuery("#contactbtn").show();
            jQuery("#sb_contentbox").addClass("highlighted");
            jQuery("#sidebar_right #sb_form .centerselect,#sidebar_right #sb_form .gfield#field_1_23").show();

        } else
        {
            jQuery(".dyn").hide();
            jQuery(".ndyn").show();
            jQuery("#contactbtn").hide();
            jQuery("#sb_contentbox").removeClass("highlighted");
            jQuery("#sidebar_right #sb_form .centerselect,#sidebar_right #sb_form .gfield#field_1_23").hide();
        }

        contact_form.chosenUpdate();



    },
    'update_center': function (rel, attr)
    {

        jQuery("#sidebar_right #input_1_23 option").each(function () {


            if (jQuery(this).val() == rel)
            {
                if (attr == true)
                {
                    jQuery(this).attr('selected', 'selected');
                } else
                {
                    jQuery(this).removeAttr('selected');
                }
            }
        });
        contact_form.chosenUpdate();



    },
    'init': function ()
    {


        /*labelpos*/
        contact_form.initLabelPos();

        /*nice select*/
        $('.gform_wrapper .ginput_container_select select').niceSelect();

        jQuery(document).bind('gform_post_render', function () {
            contact_form.init();
        });

        if ($('#input_1_23').length > 0)
        {
            $('#input_1_23').chosen();
        }

        if ($('#sidebar_right #input_1_23').length > 0)
        {
            $('#sidebar_right #input_1_23').chosen().change(function (event, deselected) {
                var relval = deselected.deselected;

                var btn = $(".centerselect[rel=" + relval + "]");
                btn.removeClass("checked");
                var chosenval = $('#sidebar_right #input_1_23').chosen().val();

                if (chosenval == null) {
                    $('#sidebar_right #sb_form .gfield#field_1_23').hide();
                }


            })
        }


        /*hide field checkbox approved*/
        jQuery("#input_1_16").hide();

        contact_form.updateStates();
        var _self = this;
        jQuery(".centerselect").click(function (event) {
            _self.updateStates();
        });

        //hide contact button on click
        jQuery("#contactbtn").on("click", function () {
            jQuery(this).hide();
        });


        jQuery(document).bind('gform_confirmation_loaded', function (event, formId) {

            var target = "#sb_form";
            var $target = $(target);
            $('html, body').animate({
                'scrollTop': ($target.offset().top) - 200
            }, 1000, 'swing');

        });



    },
    'chosenUpdate': function ()
    {
        $('#input_1_23').trigger("chosen:updated");
    }
    , 'initLabelPos': function ()
    {

        /*
         *
         * GRAVITY FORM LABEL ANIMATION
         */

        var inputphform = jQuery(".label_as_placeholder input");
        inputphform.each(function ()
        {
            if (jQuery(this).length != 0)
            {
                if (jQuery(this).val().length != 0)
                {
                    jQuery(this).parents(".gfield").find("label").addClass("sup");
                } else
                {
                    jQuery(this).parents(".gfield").find("label").removeClass("sup");
                }
            }
        });
        var textareaphform = jQuery(".label_as_placeholder textarea");
        textareaphform.each(function ()
        {
            if (jQuery(this).val())
            {
                jQuery(this).parents(".gfield").find("label").addClass("sup");
            }
        });
        jQuery(".label_as_placeholder input,.label_as_placeholder textarea").focusin(function ()
        {
            var label = jQuery(this).parents(".gfield").find("label");
            label.addClass("sup");
        }).focusout(function ()
        {
            var val = jQuery(this).val().length;
            var label = jQuery(this).parents(".gfield").find("label");
            if (val === 0)
            {
                label.removeClass("sup");
            }
        });
    }



};

jQuery(function ()
{
    contact_form.init();
});