
/*
 * Geolocalisation
 */

jQuery( function ()
{
    geoloc.init();
} );

var geoloc =
    {
	init: function ()
	{

	    var options = {
		enableHighAccuracy: true,
		timeout: 5000,
		maximumAge: 0
	    };



	    if ( navigator.geolocation ) {
		navigator.geolocation.getCurrentPosition( geoloc.enablegeoloc, geoloc.showErrors, options );
	    }
	},
	showErrors: function ( errors )
	{
	    console.log( errors );
	},
	enablegeoloc: function ( position )
	{
	    console.log( position );
	    var lng = position.coords.longitude;
	    var lat = position.coords.latitude;
	    console.log( 'longitude: ' + lng + ' | latitude: ' + lat );

	    //call ajax
	    jQuery.ajax(
		{
		    type: "POST",
		    context: this,
		    url: tbc_vars.ajaxurl,
		    data: {
			'action': 'getlocation',
			'securite_nonce': tbc_vars.securite_nonce,
			'latitude': lat,
			'longitude': lng
		    },
		    dataType: "json",
		    cache: true,
		    beforeSend: function ()
		    {

		    },
		    success: function ( msg )
		    {
			var res = JSON.stringify( msg );
			var response = JSON.parse( res );
			if ( response )
			{
			    var country = response.parent;
			    var city = response.term_id;

			    var selectedcountry = jQuery( ".home #wpv_control_select_wpv-wpcf-pays" ).val();
			    if ( country == selectedcountry )
			    {

				localisation_select.selectCity( city );
			    } else
			    {
				var callbackfn = function () {
				    localisation_select.majcities( country, city );
				};
				jQuery( ".home #wpv_control_select_wpv-wpcf-pays option" ).removeAttr( "selected" );
				localisation_select.selectCountry( country, callbackfn );

			    }
			}
		    }
		} );

	}
    }