<?php
/**
 * TBC THEME
 * By Avelook
 *
 * @package avelook
 */

/*
 * **********************************************************************************
 * UPDATE PACKAGE THEME (PLUGIN INSTALLED ON TBC MAIN SITE !)  *****************************
 *  **********************************************************************************
 */

require_once get_template_directory() . '/lib/wp-package-updater/class-wp-package-updater.php';
$wppus_bypass_themes = false;
/** Enable theme updates without license check **/
$prefix_updater = new WP_Package_Updater(
    'https://team-business-centers.com/fr',
    wp_normalize_path(__FILE__),
    get_template_directory()
);

/*
 * **********************************************************************************
 * LOADING TIME PERFORMANCE OPTIMIZATIONS *****************************
 *  **********************************************************************************
 */

/*
 * Disable the emoji's
 */
function disable_emojis()
{
    remove_action('wp_head', 'print_emoji_detection_script', 7);
    remove_action('admin_print_scripts', 'print_emoji_detection_script');
    remove_action('wp_print_styles', 'print_emoji_styles');
    remove_action('admin_print_styles', 'print_emoji_styles');
    remove_filter('the_content_feed', 'wp_staticize_emoji');
    remove_filter('comment_text_rss', 'wp_staticize_emoji');
    remove_filter('wp_mail', 'wp_staticize_emoji_for_email');
    add_filter('tiny_mce_plugins', 'disable_emojis_tinymce');
}

add_action('init', 'disable_emojis');

/**
 * Filter function used to remove the tinymce emoji plugin.
 *
 * @param    array  $plugins
 * @return   array             Difference betwen the two arrays
 */
function disable_emojis_tinymce($plugins)
{
    if (is_array($plugins)) {
        return array_diff($plugins, array('wpemoji'));
    } else {
        return array();
    }
}

// Remove comment-reply.min.js from footer
function comments_clean_header_hook()
{
    wp_deregister_script('comment-reply');
}

add_action('init', 'comments_clean_header_hook');

/*
 * Remove dashicons in frontend for unauthenticated users
 *
 */

add_action('wp_enqueue_scripts', 'bs_dequeue_dashicons');

function bs_dequeue_dashicons()
{
    if (!is_user_logged_in()) {
        wp_deregister_style('dashicons');
    }
}

/*
 * **********************************************************************************
 * OTHER FUNCTIONS  *****************************
 *  **********************************************************************************
 */

/*  CUSTOMIZE LOGIN PAGE
 *
 */

function my_custom_login()
{
    echo '<style>
        body.login{background:url("' . get_theme_mod('bg-login') . '");background-size:100% auto;}
            .login h1 a{background-image:url("' . get_theme_mod('logo') . '");
                width:100%;background-size:100%;
                height:120px;margin:0;
                }
                </style>';
}

add_action('login_head', 'my_custom_login');

add_filter('login_form_top', 'addtoploginform');

function my_login_logo_url()
{
    return get_bloginfo('url');
}

add_filter('login_headerurl', 'my_login_logo_url');

function my_login_head()
{
    remove_action('login_head', 'wp_shake_js', 12);
}

add_action('login_head', 'my_login_head');

//redirect admin
function mt_redirect_admin($redirect_to, $request, $user)
{

    if (isset($user->roles) && is_array($user->roles)) {
        //check for admins
        if (in_array('subscriber', $user->roles)) {
            wp_redirect(site_url() . '/espace-membre');
            exit;
        } else {
            return $redirect_to;
        }
    } else {
        return $redirect_to;
    }
}

add_action('login_redirect', 'mt_redirect_admin', 10, 3); //admin_init before

/*
 *
 * ADD MANUALLY ITEMS TO MAIN MENU
 */

function add_last_nav_item($items)
{
    ob_start();
    do_action('wpml_add_language_selector');
    $output = ob_get_contents();
    ob_end_clean();

    $items .= '<li class="nav-item menu-item extra d-none d-xxl-block">
        <span  id="tel_tbc" class="font-weight-bold"><span class="border-bottom ">' . get_theme_mod('phone') . '</span>
    </span></li>

            <li class="nav-item menu-item extra " id="loginpic" ><a data-toggle="modal" class="" href="#" data-target="#loginBox">
            <span class="text  d-xxl-none">' . __('Espace adhérent', 'tbc_theme') . '</span><img src="' . get_template_directory_uri() . '/assets/images/pic-member.png"></a>
        </li>
<li class="nav-item  extra d-none d-xxl-block "  id="selectlang">
                                ' . $output
        . '</li>'

    ;
    $menuLocations = get_nav_menu_locations();
    $menuID        = $menuLocations['mainmenu_extra']; // Get the *primary* menu ID
    $primaryNav    = wp_get_nav_menu_items($menuID);
    foreach ($primaryNav as $navItem) {
        $classes = '';
        foreach ($navItem->classes as $class) {
            $classes .= ' ' . $class . ' ';
        }

        $items .= '<li class=" ' . $classes . ' extra  nav-item menu-item menu-item-' . $navItem->object_id . '"><a class="nav-link" href="' . $navItem->url . '" title="' . $navItem->title . '">' . $navItem->title . '</a></li>';
    }

    return $items;
}

add_filter('wp_nav_menu_menu-principal_items', 'add_last_nav_item');

/*
 * STARTER THEME OPTIONS
 */

if (!function_exists('starter_setup')):

    function starter_setup()
{

        load_theme_textdomain('wp-bootstrap-starter', get_template_directory() . '/languages');
        add_theme_support('automatic-feed-links');
        add_theme_support('title-tag');
        add_theme_support('post-thumbnails');
        register_nav_menus(array(
            'primary' => esc_html__('Primary', 'wp-bootstrap-starter'),
        ));
        // Switch default core markup for search form, comment form, and comments
        add_theme_support('html5', array(
            'comment-form',
            'comment-list',
            'caption',
        ));
    }

endif;
add_action('after_setup_theme', 'starter_setup');

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 */
function wp_bootstrap_starter_content_width()
{
    $GLOBALS['content_width'] = apply_filters('wp_bootstrap_starter_content_width', 1170);
}

add_action('after_setup_theme', 'wp_bootstrap_starter_content_width', 0);

/*
 * REGISTER WIDGET AREA
 */

function wp_bootstrap_starter_widgets_init()
{

    /* $test = apply_filters( 'wpml_object_id', 311, 'centre', false, 'fr' );

    var_dump( $test ); */

    register_sidebar(array(
        'name'          => esc_html__('Footer 1', 'theme'),
        'id'            => 'footer-1',
        'description'   => esc_html__('Add widgets here.', 'wp-bootstrap-starter'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ));

    register_sidebar(array(
        'name'          => esc_html__('Footer 2', 'theme'),
        'id'            => 'footer-2',
        'description'   => esc_html__('Add widgets here.', 'theme'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ));

    register_sidebar(array(
        'name'          => esc_html__('Footer social', 'theme'),
        'id'            => 'footersocial',
        'description'   => esc_html__('Add widgets here.', 'theme'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<span class="hidden">',
        'after_title'   => '</span>',
    ));

    register_sidebar(array(
        'name'          => esc_html__('Tableau de bord', 'theme'),
        'id'            => 'dashboard',
        'description'   => esc_html__('Add widgets here.', 'theme'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ));

}

add_action('widgets_init', 'wp_bootstrap_starter_widgets_init');

/**
 * ENQUEUE SCRIPTS AND STYLES
 */
function starter_scripts()
{

    //scripts if private area
    if (get_page_template_slug() == 'privatearea.php') {
        wp_deregister_script('jquery');
        wp_register_script('jquery', 'https://code.jquery.com/jquery-3.3.1.min.js', false, '');
        wp_enqueue_script('jquery');

        wp_enqueue_style('typo-nimbus', 'https://use.typekit.net/ymv1sod.css');
        wp_enqueue_style('material-style-bootstrap', get_template_directory_uri() . '/assets/material-lite/assets/plugins/bootstrap/css/bootstrap.min.css');
        wp_enqueue_style('material-style', get_template_directory_uri() . '/assets/material-lite/css/style.css');
        wp_enqueue_style('material-style-blue', get_template_directory_uri() . '/assets/material-lite/css/colors/blue.css');
        wp_enqueue_style('custom-styles-css', get_template_directory_uri() . '/assets/css/privatearea.css');
        wp_enqueue_style('juistyle', '//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css');
        wp_enqueue_style('nice-select-css', get_template_directory_uri() . '/assets/material-lite/css/nice-select.css');

        wp_enqueue_style('chartist-css', get_site_url() . '/wp-content/plugins/tbc-plugin/public/css/chartist.min.css', false);
        wp_enqueue_script('jqui', 'https://code.jquery.com/ui/1.12.1/jquery-ui.js', array(), 1.0, true);
        wp_enqueue_script('tinymce', 'https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.7.13/tinymce.min.js', array(), 1.0, true);

        global $wp_query;
        $entry_id = $wp_query->get('entry');

        $data = "
var tbc_vars=
{
base_url:'" . get_site_url() . "',
 ajaxurl: '" . plugin_dir_url('') . 'tbc-plugin/admin/admin-ajax.php' . "',
securite_nonce:  '" . wp_create_nonce('securite-nonce') . "',
 puploads:'" . get_site_url() . '/wp-content/uploads/tbc/proposals/' . $entry_id . '/' . "',
 entry_id:'" . $entry_id . "',
 theme:{
  base_url: '" . get_template_directory_uri() . "'
 },
sending_proposal:
 {
 sending_process: '" . __('Envoi en cours...', 'tbc-plugin') . "',
 success_heading: '" . __('Envoi réussi', 'tbc-plugin') . "',
 success_message: '" . __('Votre proposition commerciale a bien été envoyée. Une copie a  été envoyée au service commercial de TBC. La page va se recharger, merci de patienter...', 'tbc-plugin') . "'
 }

};";
        wp_add_inline_script('jquery', $data, 'after');

/*PLUGIN TBC*/
        wp_enqueue_script('tbcpluginjs', get_site_url() . '/wp-content/plugins/tbc-plugin/public/js/tbc-public.js', array('jquery'), 1.0, false);
        wp_enqueue_script('jquery-validate', 'https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js', array('jquery'), 1.0, true);
        wp_enqueue_script('ui-widget', get_site_url() . '/wp-content/plugins/tbc-plugin/public/js/jquery.ui.widget.js', array('jquery'));
        //wp_enqueue_script('ui-widget','https://blueimp.github.io/JavaScript-Templates/js/tmpl.min.js',array ('ui-widget'));
        wp_enqueue_script('iframe-transport', get_site_url() . '/wp-content/plugins/tbc-plugin/public/js/jquery.iframe-transport.js', array('jquery', 'ui-widget'));
        wp_enqueue_script('jquery-file-upload', get_site_url() . '/wp-content/plugins/tbc-plugin/public/js/jquery.fileupload.js', array('jquery', 'ui-widget', 'iframe-transport'));
        //wp_enqueue_script('tinymce','https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.7.13/jquery.tinymce.min.js',array (),1.0,false);

        wp_enqueue_script('juicombobox', get_site_url() . '/wp-content/plugins/tbc-plugin/public/js/jquery.ui-combobox.js');
        wp_enqueue_script('front-js', get_site_url() . '/wp-content/plugins/tbc-plugin/public/js/front.js', array('jquery', 'ui-widget', 'iframe-transport', 'jquery-file-upload', 'tinymce'));

        wp_enqueue_script('dashboard-js', get_site_url() . '/wp-content/plugins/tbc-plugin/public/js/dashboard.js', array('chartist-js'));
        //wp_enqueue_script('dialogfx-js',plugin_dir_url(__FILE__).'js/dialogFx.js',array ('jquery','ui-widget'));
        //
        /**/

        wp_enqueue_script('ml-bootstrap', get_template_directory_uri() . '/assets/material-lite/assets/plugins/bootstrap/js/bootstrap.min.js', array(), 1.0, true);
        wp_enqueue_script('ml-bootstrap-teth', get_template_directory_uri() . '/assets/material-lite/assets/plugins/bootstrap/js/tether.min.js', array(), 1.0, true);
        wp_enqueue_script('nice-select-js', get_template_directory_uri() . '/assets/js/jquery.nice-select.js', array(), 1.0, true);
        wp_enqueue_script('ml-sidebarmenu', get_template_directory_uri() . '/assets/material-lite/js/sidebarmenu.js', array(), 1.0, true);
        //wp_enqueue_script('ml-wave',get_template_directory_uri().'/assets/material-lite/js/wave.js',array (),1.0,true);
        wp_enqueue_script('ml-slimscroll', get_template_directory_uri() . '/assets/material-lite/js/jquery.slimscroll.js', array(), 1.0, true);
        wp_enqueue_script('ml-sticky', get_template_directory_uri() . '/assets/material-lite/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js', array(), 1.0, true);
        wp_enqueue_script('ml-customjs', get_template_directory_uri() . '/assets/material-lite/js/custom.min.js', array(), 1.0, true);

        wp_enqueue_script('tether-js', get_template_directory_uri() . '/assets/material-lite/js/tether.min.js', array('jquery', 'ui-widget'));
        wp_enqueue_script('chartist-js', 'https://cdn.jsdelivr.net/chartist.js/latest/chartist.min.js', [], true);
        wp_enqueue_script('d3-js', get_template_directory_uri() . '/assets/material-lite/js/d3.min.js', [], true);
        wp_enqueue_script('c3-js', get_template_directory_uri() . '/assets/material-lite/js/c3.min.js', [], true);

        //add_filter('show_admin_bar','__return_false');
        //wp_enqueue_script('ml-chartlist',get_template_directory_uri().'/assets/material-lite/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js',array (),1.0,true);
    } else {

        /* CSS */
        wp_enqueue_style('typo-nimbus', 'https://use.typekit.net/ymv1sod.css');
        wp_enqueue_style('starter-bootstrap4-css', get_template_directory_uri() . '/assets/css/bootstrap.min.css');
//        wp_enqueue_style('starter-font-awesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css', false, '4.1.0');
        wp_enqueue_style('iosCheckbox-css', get_template_directory_uri() . '/assets/css/iosCheckbox.css');
        wp_enqueue_style('owl-css', get_template_directory_uri() . '/assets/css/owl-carousel.css');
        wp_enqueue_style('starter-style', get_stylesheet_uri());
        wp_enqueue_style('custom-styles-css', get_template_directory_uri() . '/assets/css/style.css');
        wp_enqueue_style('old-ie', get_stylesheet_directory_uri() . "/assets/css/ie-only.css", array('custom-styles-css'));
        wp_style_add_data('old-ie', 'conditional', 'IE');
        wp_enqueue_style('bt9-bootstrap-fix', 'https://cdn.jsdelivr.net/gh/coliff/bootstrap-ie8/css/bootstrap-ie9.min.css', array('custom-styles-css'));
        wp_style_add_data('bt9-bootstrap-fix', 'conditional', 'IE 9');
        wp_enqueue_style('bt8-bootstrap-fix', 'https://cdn.jsdelivr.net/gh/coliff/bootstrap-ie8/css/bootstrap-ie8.min.css', array('custom-styles-css'));
        wp_style_add_data('bt8-bootstrap-fix', 'conditional', 'lt IE 8');

        /* JS */
        wp_enqueue_script('jquery');
        //wp_enqueue_script( 'starter-popper', get_template_directory_uri() . '/assets/js/popper.min.js', array(), true );
        wp_enqueue_script('starter-popper', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js', array(), 1.0, true);
        //wp_enqueue_script( 'starter-bootstrapjs', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array(), false );
        wp_enqueue_script('starter-bootstrapjs', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js', array(), 1.0, true);
        //wp_enqueue_script( 'starter-skip-link-focus-fix', get_template_directory_uri() . '/assets/js/skip-link-focus-fix.js', array(), '20151215', true );
        wp_enqueue_script('starter-owl', get_template_directory_uri() . '/assets/js/owl.carousel.min.js', array(), 1.0, true);
//    wp_enqueue_script( 'starter-owl-thumbnail', get_template_directory_uri() . '/assets/js/owl.carousel.plugin.thumbnails.min', array(), 1.0, false );
        wp_enqueue_script('iosCheckbox-js', get_template_directory_uri() . '/assets/js/iosCheckbox.js', array(), 1.0, true);
        wp_enqueue_script('nice-select-js', get_template_directory_uri() . '/assets/js/jquery.nice-select.js', array(), 1.0, true);
        wp_enqueue_script('viewport-checker', get_template_directory_uri() . '/assets/js/viewportchecker.js', array(), 1.0, true);
        wp_enqueue_script('theme-script', get_template_directory_uri() . '/assets/js/theme-script.js', array(), 1.0, true);

        if (is_front_page()) {

            wp_enqueue_script('geoloc', get_template_directory_uri() . '/assets/js/geoloc.js', array(), true);
        }

        /* js ie only */
        wp_enqueue_script('starter-html5hiv', get_template_directory_uri() . '/assets/js/html5.js', array(), '3.7.0', false);
        wp_script_add_data('starter-html5hiv', 'conditional', 'IE 9');
        wp_enqueue_script('flexibility', get_template_directory_uri() . '/assets/js/flexibility.js', array(), '3.7.0', false);
        wp_script_add_data('flexibility', 'conditional', 'lt IE 11');
        wp_enqueue_script('test', get_template_directory_uri() . '/assets/js/test.js', array(), '3.7.0', false);
        wp_script_add_data('test', 'conditional', 'IE 9');

    }
}

add_action('wp_enqueue_scripts', 'starter_scripts');

function enqueue_gform_script($form, $is_ajax)
{

    wp_enqueue_script('contact_form', get_template_directory_uri() . '/assets/js/contact_form.js', array(), true);
}

add_action('gform_enqueue_scripts', 'enqueue_gform_script', 10, 2);

function wp_bootstrap_starter_password_form()
{
    global $post;
    $label = 'pwbox-' . (empty($post->ID) ? rand() : $post->ID);
    $o     = '<form action="' . esc_url(site_url('wp-login.php?action=postpass', 'login_post')) . '" method="post">
    <div class="d-block mb-3">' . __("To view this protected post, enter the password below:", "wp-bootstrap-starter") . '</div>
    <div class="form-group form-inline"><label for="' . $label . '" class="mr-2">' . __("Password:", "wp-bootstrap-starter") . ' </label><input name="post_password" id="' . $label . '" type="password" size="20" maxlength="20" class="form-control mr-2" /> <input type="submit" name="Submit" value="' . esc_attr__("Submit", "wp-bootstrap-starter") . '" class="btn btn-primary"/></div>
    </form>';
    return $o;
}

add_filter('the_password_form', 'wp_bootstrap_starter_password_form');

/**
 * Load custom WordPress nav walker.
 */
if (!class_exists('wp_bootstrap_navwalker')) {
    require_once get_template_directory() . '/inc/wp_bootstrap_navwalker.php';
    require_once get_template_directory() . '/inc/wp_bootstrap_navwalker_flat.php';
    require_once get_template_directory() . '/inc/wp_bootstrap_navwalker_submenus.php';
}

/*
 * Load Avelook theme customizer
 *
 */

if (!class_exists('AvelookTheme_Customize')) {
    require get_template_directory() . '/inc/customizer.php';
}

/*
 * TBC CUSTOM FUNCTIONS
 *
 */

function sc_theme_url()
{
    $i = get_template_directory_uri() . '/assets/images';
    return $i;
}

add_shortcode('theme-url', 'sc_theme_url');

//ajax login
require_once get_template_directory() . '/lib/custom-ajax-auth.php';

register_nav_menus(array(
    'mainmenu'       => esc_html__('Mainmenu', 'wp-bootstrap-starter'),
    'mainmenu_extra' => esc_html__('Mainmenu_extra', 'wp-bootstrap-starter'),
));

register_nav_menus(array(
    'customer_menu' => esc_html__('Menu client', 'theme'),
));

register_nav_menus(array(
    'admin_menu' => esc_html__('Menu admin front', 'theme'),
));

register_nav_menus(array(
    'member_menu' => esc_html__('Menu member front', 'theme'),
));

add_filter('gform_field_container', 'add_bootstrap_container_class', 10, 6);
function add_bootstrap_container_class($field_container, $field, $form,
    $css_class, $style, $field_content) {
    $id       = $field->id;
    $field_id = is_admin() || empty($form) ? "field_{$id}" : 'field_' . $form['id'] . "_$id";
    return '<li id="' . $field_id . '" class="' . $css_class . ' form-group">{FIELD_CONTENT}</li>';
}

/*
 * VISUAL COMPOSER REPLACE PREFIX BOOTSTRAP
 */

// Filter to replace default css class names for vc_row shortcode and vc_column
add_filter('vc_shortcodes_css_class', 'custom_css_classes_for_vc_row_and_vc_column', 10, 2);

function custom_css_classes_for_vc_row_and_vc_column($class_string, $tag)
{
    if ($tag == 'vc_row' || $tag == 'vc_row_inner') {
        $classNames = ['wpb_row', 'vc_row-fluid', 'vc_row'];

        foreach ($classNames as $className) {
            $class_string = str_replace($className, '', $class_string);
        }

        $class_string = ('row' . ($class_string ? ' ' : '') . trim($class_string)); // This will replace "vc_row-fluid" with "my_row-fluid"
    }

    if ($tag == 'vc_column' || $tag == 'vc_column_inner') {
        $classNames = ['wpb_column', 'vc_column_container'];

        foreach ($classNames as $className) {
            $class_string = str_replace($className, '', $class_string);
        }

        $cols = ['col-xs-offset-', 'col-xs-', 'col-sm-offset-', 'col-sm-', 'col-md-offset-', 'col-md-', 'col-lg-offset-', 'col-lg-'];
        foreach ($cols as $col) {
            $num = 0;

            if (preg_match('/vc_' . $col . '(\d{1,2})/', $class_string, $regs)) {
                $num = (int) str_replace(('vc_' . $col), '', $regs[0]);
            }

            $class_string = preg_replace('/vc_' . $col . '(\d{1,2})/', ($col . ($num)), $class_string); // This will replace "vc_col-sm-%" with "my_col-sm-%"
            //    $class_string = preg_replace( '/vc_' . $col . '(\d{1,2})/', ($col . ($num * ($num < 12 ? 5 : 1)) ), $class_string ); // Th
        }
    }

    return $class_string; // Important: you should always return modified or original $class_string
}

/*
 * GRAVITY FORMS UTILITIES
 */
/* Add a custom field to the field editor (See editor screenshot) */
add_action("gform_field_standard_settings", "my_standard_settings", 10, 2);

function my_standard_settings($position, $form_id)
{

// Create settings on position 25 (right after Field Label)

    if ($position == 25) {
        ?>

        <li class="admin_label_setting field_setting" style="display: list-item; ">
        <label for="field_placeholder">Placeholder Text

        <!-- Tooltip to help users understand what this field does -->
        <a href="javascript:void(0);" class="tooltip tooltip_form_field_placeholder" tooltip="&lt;h6&gt;Placeholder&lt;/h6&gt;Enter the placeholder/default text for this field.">(?)</a>

        </label>

        <input type="text" id="field_placeholder" class="fieldwidth-3" size="35" onkeyup="SetFieldProperty('placeholder', this.value);">

        </li>
        <?php
}
}

/* Now we execute some javascript technicalitites for the field to load correctly */

add_action("gform_editor_js", "my_gform_editor_js");

function my_gform_editor_js()
{
    ?>
    <script>
    //binding to the load field settings event to initialize the checkbox
    jQuery(document).bind("gform_load_field_settings", function(event, field, form){
    jQuery("#field_placeholder").val(field["placeholder"]);
    });
    </script>

    <?php
}

/* We use jQuery to read the placeholder value and inject it to its field */

add_action('gform_enqueue_scripts', "my_gform_enqueue_scripts", 10, 2);

function my_gform_enqueue_scripts($form, $is_ajax = false)
{

}

add_filter('gform_enable_field_label_visibility_settings', '__return_true');

add_shortcode('wpv-post-param', 'wpv_post_param_shortcode');

function wpv_post_param_shortcode($atts)
{

    if (!empty($atts['var'])) {

        if (isset($_GET[$atts['var']])) {
            $var = (array) $_GET[$atts['var']];

            if (empty($var[0])) {
                return "0";
            }
            return esc_html(implode(', ', $var));
        } else {
            return "-1";
        }
    }
}

/*
 * restrict privatearea access
 */

function template_redirect_fn()
{

    if (!(is_user_logged_in()) && get_page_template_slug() == 'privatearea.php') {

        $redirect = site_url() . '/wp-login.php?redirect_to=' . urlencode($_SERVER['REQUEST_URI']);
        wp_redirect($redirect);

        /* wp_redirect( home_url() . '/no-access' ); */
        exit;
    }
}

add_action('template_redirect', 'template_redirect_fn');

/*
 * has child terms
 */
function hasChildTerms($ville)
{

    $arr = get_term_children($ville, 'localisation');
    if (!empty($arr)) {
        return true;
    } else {
        return false;
    }
}

add_action('init', 'hasChildTerms');

/*
 * PLANYO
 */

add_shortcode('planyo_embed', 'embedPlanyo');

function embedPlanyo()
{

    $attrs = "";
    if (isset($_GET['mode'])) {
        $attrs .= "mode=" . $_GET['mode'];
    }

    if (isset($_GET['ppp_resfilter'])) {
        $attrs .= "&ppp_resfilter=" . $_GET['ppp_resfilter'];
    }

    if (isset($_GET['sort'])) {
        $attrs .= "&sort=" . $_GET['sort'];
    }

    return do_shortcode("[planyo attribute_string='" . $attrs . "'] ");
}

/*
 * BREADCRUMB
 */

function the_breadcrumb()
{

    global $post;
//schema link
    $schema_link = 'http://data-vocabulary.org/Breadcrumb';
    $home        = 'Home';
    $delimiter   = ' > ';
    $homeLink    = get_bloginfo('url');
    if (is_home() || is_front_page()) {
// no need for breadcrumbs in homepage
    } else {
        echo '<div class="breadcrumb">';
// main breadcrumbs lead to homepage
        /*     if ( !is_single() ) {
        echo __( 'Vous êtes ici', 'tbc_theme' );
        } */
        //echo '<span itemscope itemtype="' . $schema_link . '"><a itemprop="url" href="' . $homeLink . '">' . '<span itemprop="title">' . $home . '</span>' . '</a></span>' . $delimiter . ' ';
        // if blog page exists
        if (get_page_by_path('blog')) {
            if (!is_page('blog')) {
                echo '<span itemscope itemtype="' . $schema_link . '"><a itemprop="url" href="' . get_permalink(get_page_by_path('blog')) . '">' . '<span itemprop="title">Blog</span></a></span>' . $delimiter . ' ';
            }
        }
        if (is_category()) {
            $thisCat = get_category(get_query_var('cat'), false);
            if ($thisCat->parent != 0) {
                $category_link = get_category_link($thisCat->parent);
                echo '<span itemscope itemtype="' . $schema_link . '"><a itemprop="url" href="' . $category_link . '">' . '<span itemprop="title">' . get_cat_name($thisCat->parent) . '</span>' . '</a></span>' . $delimiter . ' ';
            }
            $category_id   = get_cat_ID(single_cat_title('', false));
            $category_link = get_category_link($category_id);
            echo '<span itemscope itemtype="' . $schema_link . '"><a itemprop="url" href="' . $category_link . '">' . '<span itemprop="title">' . single_cat_title('', false) . '</span>' . '</a></span>';
        } elseif (is_single() && !is_attachment()) {
            if (get_post_type() != 'post') {
                $post_type = get_post_type_object(get_post_type());
                $slug      = $post_type->rewrite;
                echo '<span itemscope itemtype="' . $schema_link . '"><a itemprop="url" href="' . $homeLink . '/' . $slug['slug'] . '">' . '<span itemprop="title">' . $post_type->labels->singular_name . '</span>' . '</a></span>';
                echo ' ' . $delimiter . ' ' . get_the_title();
            } else {
                $category = get_the_category();
                if ($category) {
                    foreach ($category as $cat) {
                        echo '<span itemscope itemtype="' . $schema_link . '"><a itemprop="url" href="' . get_category_link($cat->term_id) . '">' . '<span itemprop="title">' . $cat->name . '</span>' . '</a></span>' . $delimiter . ' ';
                    }
                }
                echo get_the_title();
            }
        } elseif (!is_single() && !is_page() && get_post_type() != 'post' && !is_404()) {
            $post_type = get_post_type_object(get_post_type());
            echo $post_type->labels->singular_name;
        } elseif (is_attachment()) {
            $parent = get_post($post->post_parent);
            $cat    = get_the_category($parent->ID);
            $cat    = $cat[0];
            echo get_category_parents($cat, true, ' ' . $delimiter . ' ');
            echo '<span itemscope itemtype="' . $schema_link . '"><a itemprop="url" href="' . get_permalink($parent) . '">' . '<span itemprop="title">' . $parent->post_title . '</span>' . '</a></span>';
            echo ' ' . $delimiter . ' ' . get_the_title();
        } elseif (is_page() && !$post->post_parent) {
            $get_post_slug = $post->post_name;
            $post_slug     = str_replace('-', ' ', $get_post_slug);
            echo '<span itemscope itemtype="' . $schema_link . '"><a itemprop="url" href="' . get_permalink() . '">' . '<span itemprop="title">' . ucfirst($post_slug) . '</span>' . '</a></span>';
        } elseif (is_page() && $post->post_parent) {
            $parent_id   = $post->post_parent;
            $breadcrumbs = array();
            while ($parent_id) {
                $page          = get_page($parent_id);
                $breadcrumbs[] = '<span itemscope itemtype="' . $schema_link . '"><a itemprop="url" href="' . get_permalink($page->ID) . '">' . '<span itemprop="title">' . get_the_title($page->ID) . '</span>' . '</a></span>';
                $parent_id     = $page->post_parent;
            }
            $breadcrumbs = array_reverse($breadcrumbs);
            for ($i = 0; $i < count($breadcrumbs); $i++) {
                echo $breadcrumbs[$i];
                if ($i != count($breadcrumbs) - 1) {
                    echo ' ' . $delimiter . ' ';
                }

            }
            echo $delimiter . '<span itemscope itemtype="' . $schema_link . '"><a itemprop="url" href="' . get_permalink() . '">' . '<span itemprop="title">' . the_title_attribute('echo=0') . '</span>' . '</a></span>';
        } elseif (is_tag()) {
            $tag_id = get_term_by('name', single_cat_title('', false), 'post_tag');
            if ($tag_id) {
                $tag_link = get_tag_link($tag_id->term_id);
            }
            echo '<span itemscope itemtype="' . $schema_link . '"><a itemprop="url" href="' . $tag_link . '">' . '<span itemprop="title">' . single_cat_title('', false) . '</span>' . '</a></span>';
        } elseif (is_author()) {
            global $author;
            $userdata = get_userdata($author);
            echo '<span itemscope itemtype="' . $schema_link . '"><a itemprop="url" href="' . get_author_posts_url($userdata->ID) . '">' . '<span itemprop="title">' . $userdata->display_name . '</span>' . '</a></span>';
        } elseif (is_404()) {
            echo 'Error 404';
        } elseif (is_search()) {
            echo 'Search results for "' . get_search_query() . '"';
        } elseif (is_day()) {
            echo '<span itemscope itemtype="' . $schema_link . '"><a itemprop="url" href="' . get_year_link(get_the_time('Y')) . '">' . '<span itemprop="title">' . get_the_time('Y') . '</span>' . '</a></span>' . $delimiter . ' ';
            echo '<span itemscope itemtype="' . $schema_link . '"><a itemprop="url" href="' . get_month_link(get_the_time('Y'), get_the_time('m')) . '">' . '<span itemprop="title">' . get_the_time('F') . '</span>' . '</a></span>' . $delimiter . ' ';
            echo '<span itemscope itemtype="' . $schema_link . '"><a itemprop="url" href="' . get_day_link(get_the_time('Y'), get_the_time('m'), get_the_time('d')) . '">' . '<span itemprop="title">' . get_the_time('d') . '</span>' . '</a></span>';
        } elseif (is_month()) {
            echo '<span itemscope itemtype="' . $schema_link . '"><a itemprop="url" href="' . get_year_link(get_the_time('Y')) . '">' . '<span itemprop="title">' . get_the_time('Y') . '</span>' . '</a></span>' . $delimiter . ' ';
            echo '<span itemscope itemtype="' . $schema_link . '"><a itemprop="url" href="' . get_month_link(get_the_time('Y'), get_the_time('m')) . '">' . '<span itemprop="title">' . get_the_time('F') . '</span>' . '</a></span>';
        } elseif (is_year()) {
            echo '<span itemscope itemtype="' . $schema_link . '"><a itemprop="url" href="' . get_year_link(get_the_time('Y')) . '">' . '<span itemprop="title">' . get_the_time('Y') . '</span>' . '</a></span>';
        }
        if (get_query_var('paged')) {
            if (is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author()) {
                echo ' (';
            }

            echo __('Page', 'tbc_theme') . ' ' . get_query_var('paged');
            if (is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author()) {
                echo ')';
            }

        }
        echo '</div>';
    }
}

add_filter('get_the_archive_title', function ($title) {

    if (is_category()) {

        $title = single_cat_title('', false);
    } elseif (is_tag()) {

        $title = single_tag_title('', false);
    } elseif (is_author()) {

        $title = '<span class="vcard">' . get_the_author() . '</span>';
    }

    return $title;
});

/*
 * FOOTER
 */

function copyright_avelook()
{
    if (is_front_page()) {
        return ' |  <a href="http://www.avelook.fr">Site conçu et développé par avelook.fr</a>';
    }
}

add_shortcode('tbc_avelook', 'copyright_avelook');

/*
 * Remove margin top debug bar
 */

add_action('get_header', 'my_filter_head');
function my_filter_head()
{
    remove_action('wp_head', '_admin_bar_bump_cb');
}
add_action('after_setup_theme', 'remove_admin_bar');
function remove_admin_bar()
{
    if (!current_user_can('administrator') && !is_admin()) {
        show_admin_bar(false);
    }
}
/*
 * UNLOAD PLUGINS, DEQUEUE SCRIPTS
 */

function remove_plugins()
{

    if (is_front_page()) {

        wp_dequeue_script('jquery-geocomplete');
        wp_dequeue_script('views-addon-maps-preview-script');
        wp_dequeue_script('views-addon-maps-dialogs-script');
        wp_dequeue_script('google-maps');

    }
}

add_action('wp_head', 'remove_plugins', 1);

$request_uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);

$is_admin = strpos($request_uri, '/wp-admin/');
// add filter in front pages only
if (false === $is_admin) {

    /* add_filter( 'option_active_plugins', unloadplugins ); */
}

function unloadplugins($plugins)
{
    global $request_uri;
    $is_contact_page     = strpos($request_uri, '/contact/');
    $unnecessary_plugins = array();
    // conditions
    // if this is not contact page
    // deactivate plugins
    if (false === $is_contact_page) {
        $unnecessary_plugins[] = 'toolset-maps/toolset-maps-loader.php';
        $unnecessary_plugins[] = 'planyo/planyo.php';
        $unnecessary_plugins[] = 'gravityview/gravityview.php';
        $unnecessary_plugins[] = 'travelpayouts/travelpayouts.php';
    }

    foreach ($unnecessary_plugins as $plugin) {
        $k = array_search($plugin, $plugins);
        if (false !== $k) {
            unset($plugins[$k]);
        }
    }

    return $plugins;
}

/*
 * No index no follow on search pages
 * asked by cybercite
 */

function theme_header_metadata()
{

    global $post;

    // Page conditional if needed
    if (in_array($post->ID, [46, 137, 147, 145])):
    ?>
    <meta name="robots" content="noindex,nofollow" />

            <?php
endif;
}

add_action('wp_head', 'theme_header_metadata');

/*
 *
 * CREATE A NEW USER ROLE : CUSTOMER
 *
 */

$result = add_role(
    'customer', __('Client'), array(
        'read'         => false, // true allows this capability
        'edit_posts'   => false,
        'delete_posts' => false, // Use false to explicitly deny
    )
);