=== Theme TBC-THEME ===
Theme developped by Avelook

== Customizer ==
You have to customize theme options (see backend)
- logo
- background image for login page
- google analytics tag

== Description ==
2 page layouts :
- default (theme basics)
- private (customized layout + custom css in css folder)

