<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

get_header();


$background = 'background-image: url(' . get_template_directory_uri() . '/assets/images/bg-header-inner.jpg) !important';
?>

<section id="primary" class="">
	<main id="main" class="site-main entry-content inpage" role="main">

		<?php
		if ( have_posts() ) : ?>

			<header class="banner inner entry-header" style="<?php echo $background; ?>">
				<div class="container">
					<?php
					the_archive_title( '<h1 class="page-title">', '</h1>' );
					the_archive_description( '<div class="archive-description">', '</div>' );
					?>
				</div>
			</header><!-- .entry-header -->

			<div class="container maincontent">

					<?php
					/* Start the Loop */
			while ( have_posts() ) : the_post();

				/*
				 * Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */
				get_template_part( 'template-parts/content-itemlist', get_post_format() );

	endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>
	</div>
	</main><!-- #main -->
	</section><!-- #primary -->

<?php
 //get_sidebar();
get_footer();
